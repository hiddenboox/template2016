package com.gft.digitalbank.exchange.solution;

import com.gft.digitalbank.exchange.Exchange;
import com.gft.digitalbank.exchange.listener.ProcessingListener;
import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.model.Transaction;

import javax.jms.ConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Your solution must implement the {@link Exchange} interface.
 */
public class StockExchange implements Exchange {
    private ProcessingListener processingListener;
    private Set<Transaction> transactionSet;
    private Set<OrderBook> orderBookSet;
    private Set<String> destinationsSet;

    public StockExchange() {
        this.destinationsSet = new HashSet<>();
        this.transactionSet = new HashSet<>();
        this.orderBookSet = new HashSet<>();
    }

    @Override
    public void register(ProcessingListener processingListener) {
        this.processingListener = processingListener;
    }

    private void addDestination(String destination) {
        this.destinationsSet.add(destination);
    }

    @Override
    public void setDestinations(List<String> list) {
        list.forEach(this::addDestination);
    }

    private Set<String> getDestinationsSet() {
        return this.destinationsSet;
    }

    private void connectToQueue(String queueName) {
        ConnectionFactory connectionFactory;
        Context context;
        try {
            context = new InitialContext();
            System.out.println(queueName);
            connectionFactory = (ConnectionFactory) context.lookup("ConnectionFactory");
            System.out.println(connectionFactory);
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start() {
        this.getDestinationsSet().forEach(this::connectToQueue);
        System.out.println("Start");
    }
}
